import os
import openpyxl

workbook = openpyxl.load_workbook("adder_automation.xlsx")
sheet_name = workbook.sheetnames
sheet1 = workbook['adder']
data = sheet1['B3'].value
loc = sheet1['B2'].value
print(data)
print(loc)

x = "Window"
y = "Ubuntu"
system = sheet1['B1'].value
print(system)

if x == system:
    os.system(f'RD /S /Q {loc}\\automation_adder\\Report123')
    os.system(f"pytest -v -s -k {data} --alluredir=Report123")
    os.system(f'Xcopy {loc}\\automation_adder\\allure-report /E {loc}\\automation_adder\\Report123')
    # os.system('allure serve Report123')

elif y == system:
    os.system(f'rm -r {loc}/automation_adder/Report123')
    os.system(f"pytest -v -s -k {data} --alluredir=Report123")
    os.system(f'cp -R {loc}/automation_adder/allure-report/* {loc}/automation_adder/Report123')
    # os.system("allure serve Report123")

else:
    print("Please choose the operating system wisely")
