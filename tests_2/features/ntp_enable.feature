@ntp @alltestcases
Feature: AIM-NTP

@ntp
Scenario: Verify the functionality of AIM-NTP
  Then user verify dashboard heading
  Then user click on dashboard
  Then user verify setting option
  Then user click on setting option and verify setting page is open or not

##### network setting #####

  Then user verify network option
  Then user click on network option
  Then user click on static checkbox
  Given user enter aim ip address 2 "adder"
  Given user enter netmask "adder"
  Given user enter dns server ip addresses "adder"
  Given user enter gateway ip address "adder"
  Then user verify the save button
  Then user save the details
  Then user verify save done or not

##### ntp details ######

  Then user verify time option
  Then user click on time option
  Then user enable NTP option
#  Then user click on set ntp server 1
  Then user enter server one address "adder"
  Then user set time zone area "adder"
  Then user set time zone location "adder"
  Then user verify the save button
  Then user save the details
  Then user verify save done or not
  Then user verify date and time change or not
  Then user click on dashboard








#Scenario: user set the date and time when ntp disable
#  Then user verify setting option
#  Then user click on setting option and verify setting page is open or not
#  Then user verify time option
#  Then user click on time option
#  Then user disable NTP option
#  Then user add date day
#  Then user add date month
#  Then user add date year "2020"
#  Then user add time in hour
#  Then user add time in minute
#  Then user select time zone area
#  Then user select time zone location
#  Then user verify the save button
#  Then user save the details
#  Then user verify save done or not
#  Then user verify date and time change or not
