@sys @alltestcases
Feature: we are working on Aim box and enable / disable syslog

@sys
Scenario: user verify and click the dashboard and perform opertaion
  Then user verify dashboard heading
  Then user click on dashboard
  Then user verify setting option
  Then user click on setting option and verify setting page is open or not
  Then user verify network option
  Then user click on network option
  Then user enable the syslog
  Then user verify the save button
  Then user save the details
  Then user verify save done or not
  Then user click on dashboard

#Scenario: user disable the syslog
#  Then user verify setting option
#  Then user click on setting option and verify setting page is open or not
#  Then user verify network option
#  Then user click on network option
#  Then user disable the syslog
#  Then user verify the save button
#  Then user save the details
#  Then user verify save done or not
#  Then user click on dashboard
