@rxreboot @alltestcases
Feature: Aim: Reboot transmitter

@rxreboot
Scenario: reboot the Aim devices : Rx
  Then user verify receiver heading
  Then user click on receiver and verify the receiver page is open or not
  Then user click on the reboot option
  Then user verify reboot button is appear or not
  Then user click on reboot button
  Then user verify dashboard heading
  Then user click on dashboard