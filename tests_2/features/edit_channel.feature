@editc @alltestcases
Feature: Channel Creation

############ channel create #####################
@editc
Scenario: functionlity the edit channel
  Then user verify channel heading
  Then user click on channel and verify the channel page is open or not
  Then user click on the edit channel
  Then user add channel name "adder"
  Then user add description "adder"
  Then user add location "adder"
  Then user select video1
#  Then user select video2
  Then user select audio
  Then user select usb
#  Then user send the channel group not a member of to member of
#  Then user allow the permission of single user
#  Then user disselect the permission of single user
#  Then user allow the permission of all the user
#  Then user selected the user group permission not set to permitted
  Then user verify the save button
  Then user save the details

######### channel connect #############
  Then user verify receiver heading
  Then user click on receiver and verify the receiver page is open or not
  Then user click on connect to a channel
#  Then user verify shared button is present or not

  Then user click on "adder"
#
  Then user click on connect to a channel
  Then user verify the channel is connect or not
  Then user verify dashboard heading
  Then user click on dashboard
  Then user verify active connection heading
  Then user click on active connection tab
  Then user verify the channel name is same which we created or not in active connection
  Then user verify channel is connect with same reciever or not in active connection

  Then user verify dashboard heading
  Then user click on dashboard
  Then user verify connection log heading
  Then user click on connection log tab
  Then user verify the channel name is same which we created or not in connection log
  Then user verify channel is connect with same reciever or not in connection log

  Then user verify receiver heading
  Then user click on receiver and verify the receiver page is open or not
  Then user click on channel disconnect
  Then user verify dashboard heading
  Then user click on dashboard


########## Delete the channel #################

#@build
##Scenario: user delete the channel which is created and land on dashboard
#  Then user verify channel heading
#  Then user click on channel and verify the channel page is open or not
#  Then user delete the channel which is created
#  Then user verify delete option is occur or not
#  Then user click on the delete option
#  Then user verify dashboard heading
#  Then user click on dashboard
#

