@batch @alltestcases
Feature: we are working on Aim box create and delete the multiple channel together

############ channel create #####################

Scenario: user created channel and check the batch delete functionality
  Then user verify channel heading
  Then user click on channel and verify the channel page is open or not
  Then user verify the add channel option
  Then user click on the add channel option
  Then user add channel name "adder"
  Then user add description "adder"
  Then user add location "adder"
  Then user select video1
#  Then user select video2
  Then user select audio
  Then user select usb
  Then user send the channel group not a member of to member of
  Then user allow the permission of single user
  Then user disselect the permission of single user
  Then user allow the permission of all the user
#  Then user selected the user group permission not set to permitted
  Then user verify the save button
  Then user save the details


############## channel create second time #####################

##Scenario: user created channel second time
#  Then user verify channel heading
#  Then user click on channel and verify the channel page is open or not
#  Then user verify the add channel option
#  Then user click on the add channel option
#  Then user add channel name "adder_channel2"
#  Then user add description "automation_adder2"
#  Then user add location "wavenet2"
#  Then user select video1
##  Then user select video2
#  Then user select audio
#  Then user select usb
#  Then user send the channel group not a member of to member of
#  Then user allow the permission of single user
#  Then user disselect the permission of single user
#  Then user allow the permission of all the user
##  Then user selected the user group permission not set to permitted
#  Then user verify the save button
#  Then user save the details


Scenario: user select the batch mode and delete all the channel
  Then user verify delete batch mode option is present or not
  Then user click on the batch mode option
  Then user click on the manage checkbox
  Then user verify delete selected option is present or not
  Then user click on the delete selected option
  Then user verify delete option is occur or not
  Then user click on the delete option
  Then user click on dashboard

