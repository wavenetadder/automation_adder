@static @alltestcases
Feature: we are working on Aim box and allocate the static ip

@static
Scenario: user verify the functionality of static ip
  Then user verify dashboard heading
  Then click on dashboard
  Then user verify setting option
  Then user click on setting option and verify setting page is open or not
  Then user verify network option
  Then user click on network option
  Then user click on static checkbox
  Then user enter aim ip address "adder"
  Then user enter netmask "adder"
  Then user enter the dns server ip address "adder"
  Then user verify the save button
  Then user save the details
  Then user verify save done or not
  Then user verify the ip by login "adder"
  Then user verify dashboard heading
  Then click on dashboard

