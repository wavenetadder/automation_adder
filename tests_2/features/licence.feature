@licence @alltestcases
Feature: licence key to upgrade device limit

@licence
Scenario: verify the functionality licence key to upgrade device limit
  Then user verify dashboard heading
  Then user click on dashboard
  Then user verify the device count supported on aim "adder"
  Then user verify setting heading
  Then user click on setting option and verify the setting page is open
  Then user verify the general heading
  Then user click on general option
  Then user click on the upgarde licence link
  Given user enter the licence key "adder"
  Then user verify the save button
  Then user save the details
  Then user verify licence is valid or not

#@licence
#Scenario: user verify the device count supported on aim
  Then user verify dashboard heading
  Then user click on dashboard
  Then user again verify the device count supported on aim "24"
