import openpyxl
from pytest_bdd import scenario, given, when, then, scenarios, parsers
import allure
import time
from pagelocators.adder_common import *
from tests_1.testcases.WebDeiverFactory import driver


scenarios('../features/rx_reboot.feature')


@then("user verify receiver heading")
def user_verify_reciver_heading():

    expected = 'RECEIVERS'

    actual = driver.find_element_by_xpath(receiver).text

    if expected == actual:
        print("channel display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("channel not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False

@then("user click on receiver and verify the receiver page is open or not")
def user_click_on_receiver_and_verify_the_receiver_page_is_open_or_not():
    driver.find_element_by_xpath(receiver).click()
    print("clicked on receiver")
    time.sleep(2)
    try:
        assert "t=rx" in driver.current_url
    finally:
        if (AssertionError):
            allure.attach(driver.get_screenshot_as_png(), name="Invalid Credential",
                          attachment_type=allure.attachment_type.PNG)


@then("user click on the reboot option")
def user_click_on_the_reboot_option():
    workbook = openpyxl.load_workbook("adder_automation.xlsx")
    sheet1 = workbook['adder']
    data = sheet1['B60'].value
    driver.find_element_by_xpath(f"//td[text()='{data}']/../td[12]/span[2]/a[1]/img").click()
    print("click on the reboot options")
    time.sleep(5)


@then("user verify reboot button is appear or not")
def user_verify_reboot_button_is_appear_or_not():

    expected = 'Reboot'
    actual = driver.find_element_by_xpath('//*[@id="reboot_device_confirm_link"]').text
    if expected == actual:
        print("reboot display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                          attachment_type=allure.attachment_type.PNG)

    else:
        print("reboot not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                          attachment_type=allure.attachment_type.PNG)
        assert False


@then("user click on reboot button")
def user_click_on_reboot_button():
    driver.find_element_by_xpath('//*[@id="reboot_device_confirm_link"]/span/img').click()
    print("click on reboot button done")
    time.sleep(70)


@then("user verify dashboard heading")
def user_verify_channel_heading():
    expected = 'DASHBOARD'

    actual = driver.find_element_by_xpath(dashboard).text

    if expected == actual:
        print("dashboard display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("dashboard not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False


@then("user click on dashboard")
def user_click_on_dashboard():
    driver.find_element_by_xpath(dashboard).click()
    print("clicked on dashboard")
    time.sleep(3)
