from pytest_bdd import given, then, scenarios, parsers
import allure
import time
from pagelocators.adder_common import *
from pagelocators.testcase5 import *
from tests_1.testcases.WebDeiverFactory import driver
import openpyxl


scenarios('../features/licence.feature')


@then("user verify dashboard heading")
def user_verify_channel_heading():
    expected = 'DASHBOARD'

    actual = driver.find_element_by_xpath(dashboard).text

    if expected == actual:
        print("dashboard display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("dashboard not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False


@then("user click on dashboard")
def user_click_on_dashboard():
    driver.find_element_by_xpath(dashboard).click()
    print("clicked on dashboard")
    allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                  attachment_type=allure.attachment_type.PNG)
    time.sleep(5)
    driver.execute_script("window.scrollTo(0,500)")


@then(parsers.parse('user verify the device count supported on aim "{d_count}"'))
def user_verify_the_device_count_supported_on_aim(d_count):
    workbook = openpyxl.load_workbook("adder_automation.xlsx")
    sheet1 = workbook[d_count]
    data = sheet1['B45'].value
    b = f'{data}'
    print(b)
    # data = "24"
    expected = b

    actual = driver.find_element_by_xpath('//*[@id="footer_version"]').text
    print(actual)
    real = actual[21:-20]
    print(real)
    if expected == real:
        print("device count is display properly")
        print("device count number is :", real)
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("device count number is not display properly")
        print("device count number is :", actual)
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False

@then("user verify setting heading")
def user_verify_the_setting_heading():

    expected ="Settings"

    actual = driver.find_element_by_xpath(setting).text
    if expected == actual:
        print("setting option display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("setting option not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False

@then("user click on setting option and verify the setting page is open")
def user_click_on_setting_option_and_verify_update_page():
    driver.find_element_by_xpath(setting).click()
    try:
        assert "settings" in driver.current_url
    finally:
        if (AssertionError):
            allure.attach(driver.get_screenshot_as_png(), name="Invalid Credential",
                          attachment_type=allure.attachment_type.PNG)
            time.sleep(2)

@then("user verify the general heading")
def user_verify_the_general_heading():

    expected ="General"

    actual = driver.find_element_by_xpath(general_heading).text
    if expected == actual:
        print("general option display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("general option not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False

@then("user click on general option")
def user_click_on_general_option():
    driver.find_element_by_xpath(general_heading).click()
    print("cicked on general setting")
    driver.execute_script("window.scrollTo(0,600)")

@then("user click on the upgarde licence link")
def user_click_on_the_upgarde_licence_link():
    driver.find_element_by_xpath(licence_key_option).click()
    print("cicked on upgrade licence link")

@given(parsers.parse('user enter the licence key "{key}"'))
def user_enter_the_licence_key(key):
    workbook = openpyxl.load_workbook("adder_automation.xlsx")
    sheet1 = workbook[key]
    data = sheet1['B46'].value
    licence_key = driver.find_element_by_xpath(enter_licence_key).clear()
    licence_key = driver.find_element_by_xpath(enter_licence_key).send_keys(data)
    time.sleep(3)

@then("user verify the save button")
def user_verify_the_save_button():
    expected = "Save"

    actual = driver.find_element_by_xpath('//*[@id="admin_body"]/div/a[1]').text

    if expected == actual:
        print("save display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("save is not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False

@then("user save the details")
def user_save_the_details():
    driver.find_element_by_xpath(licence_save).click()
    time.sleep(2)


@then("user verify licence is valid or not")
def user_verify_licence_is_valid_or_not():
    expected = "Invalid licence key"

    actual = driver.find_element_by_id(v_licence_key).text
    if expected == actual:
        print("Invalid licence key")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

        assert False

    else:
        print("valid licence Key")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)


@then(parsers.parse('user again verify the device count supported on aim "{count}"'))
def user_verify_the_device_count_supported_on_aim(count):

    workbook = openpyxl.load_workbook("adder_automation.xlsx")
    sheet1 = workbook[count]
    data = sheet1['B47'].value
    x = f'{data}'
    expected = x

    actual = driver.find_element_by_xpath(device_count).text
    real = actual[21:-20]
    print(real)
    # import pdb; pdb.set_trace()
    if expected == real:
        print("device count is display properly")
        print("device count number is :", real)
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("device count number is not display properly")
        print("device count number is :", actual)
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False

