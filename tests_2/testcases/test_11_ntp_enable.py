import time
from datetime import datetime
from pytest_bdd import given, then, scenarios, parsers
import allure
from selenium.webdriver.support.select import Select
from pagelocators.adder_common import *
from pagelocators.testcase4 import *
from tests_1.testcases.WebDeiverFactory import driver
import openpyxl


scenarios('../features/ntp_enable.feature')


@then("user verify dashboard heading")
def user_verify_channel_heading():
    expected = 'DASHBOARD'

    actual = driver.find_element_by_xpath(dashboard).text

    if expected == actual:
        print("dashboard display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("dashboard not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False

@then("user click on dashboard")
def user_click_on_dashboard():
    driver.find_element_by_xpath(dashboard).click()
    print("clicked on dashboard")
    time.sleep(2)


@then("user verify setting option")
def user_verify_setting_option():

    expected = 'Settings'

    actual = driver.find_element_by_xpath(setting).text

    if expected == actual:
        print("setting display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("setting is not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False

@then("user click on setting option and verify setting page is open or not")
def user_click_on_setting_option_and_verify_setting_page_is_open_or_not():

    driver.find_element_by_xpath(setting).click()
    print("clicked on setting")
    time.sleep(2)
    try:
        assert "setting" in driver.current_url
    finally:
        if (AssertionError):
            allure.attach(driver.get_screenshot_as_png(), name="Invalid Credential",
                          attachment_type=allure.attachment_type.PNG)


@then("user verify network option")
def user_verify_network_option():

    expected = 'Network'

    actual = driver.find_element_by_xpath(network).text

    if expected == actual:
        print("network display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("network is not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False

@then("user click on network option")
def user_click_on_network_option():
    driver.find_element_by_xpath(network_option).click()
    print("clicked on network option")
    time.sleep(2)
    driver.execute_script("window.scrollTo(0,400)")


@then("user click on static checkbox")
def user_click_on_static_checkbox():
    driver.find_element_by_xpath(static_checkbox).click()
    print("clicked on static checkbox")
    allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                  attachment_type=allure.attachment_type.PNG)
    # driver.execute_script("window.scrollTo(0,1000)")



@given(parsers.parse('user enter aim ip address 2 "{ip_address_2}"'))
def user_enter_aim_ip_address_2(ip_address_2):
    workbook = openpyxl.load_workbook("adder_automation.xlsx")
    sheet1 = workbook[ip_address_2]
    data = sheet1['B35'].value
    ipaddress2 = driver.find_element_by_xpath(aim_ip_address).clear()
    ipaddress2 = driver.find_element_by_xpath(aim_ip_address).send_keys(data)
    print("aim ip address 2 entered")
    time.sleep(2)


@given(parsers.parse('user enter netmask "{net}"'))
def user_enter_netmask(net):
    workbook = openpyxl.load_workbook("adder_automation.xlsx")
    sheet1 = workbook[net]
    data = sheet1['B36'].value
    netmask = driver.find_element_by_xpath(aim_netmask).clear()
    netmask = driver.find_element_by_xpath(aim_netmask).send_keys(data)
    print("netmask entered")
    time.sleep(2)


@given(parsers.parse('user enter dns server ip addresses "{dns}"'))
def user_enter_dns_server_ip_addresses(dns):
    workbook = openpyxl.load_workbook("adder_automation.xlsx")
    sheet1 = workbook[dns]
    data = sheet1['B37'].value
    serverip = driver.find_element_by_xpath(dns_server).clear()
    serverip = driver.find_element_by_xpath(dns_server).send_keys(data)
    print("dns ip entered")
    time.sleep(2)


@given(parsers.parse('user enter gateway ip address "{gateway}"'))
def user_should_enter_username(gateway):
    workbook = openpyxl.load_workbook("adder_automation.xlsx")
    sheet1 = workbook[gateway]
    data = sheet1['B38'].value
    gatewayip = driver.find_element_by_xpath(aim_gateway).clear()
    gatewayip = driver.find_element_by_xpath(aim_gateway).send_keys(data)
    print("gateway entered")
    time.sleep(2)

@then("user verify the save button")
def user_verify_the_save_button():
    expected = "Save"

    actual = driver.find_element_by_xpath(save).text
    if expected == actual:
        print("save display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("save is not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False

@then("user save the details")
def user_save_the_details():
    driver.find_element_by_xpath(save).click()
    print('clicked on save button')
    time.sleep(2)


@then("user verify save done or not")
def user_verify_save_done_or_not():

    expected_message = "Settings updated"

    actual_message = driver.find_element_by_xpath(setting_update_msg).text
    if expected_message == actual_message:
        print("save message is display properly")
        print("text is :", actual_message)
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot', attachment_type=allure.attachment_type.PNG)
        time.sleep(1)

    else:
        print("save message is  not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False


@then("user verify time option")
def user_verify_time_option():

    expected = 'Time'

    actual = driver.find_element_by_xpath('//*[@id="tabs"]/li[6]').text

    if expected == actual:
        print("time display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("time is not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False

@then("user click on time option")
def user_click_on_network_option():
    driver.find_element_by_xpath('//*[@id="tabs"]/li[6]').click()
    print("clicked on time option")
    time.sleep(2)


@then("user enable NTP option")
def user_enabel_NTP_option():
    driver.find_element_by_xpath(ntp_enable).click()

@then("user click on set ntp server 1")
def user_click_on_set_ntp_server_1():
    driver.find_element_by_xpath(ntp_server).click()
    print("click on server 1 done")


@then(parsers.parse('user enter server one address "{address}"'))
def user_enter_server_one_address(address):
    workbook = openpyxl.load_workbook("adder_automation.xlsx")
    sheet1 = workbook[address]
    data = sheet1['B39'].value
    driver.find_element_by_xpath(nto_serverone_address).clear()
    driver.find_element_by_xpath(nto_serverone_address).send_keys(data)

@then(parsers.parse('user set time zone area "{area}"'))
def user_set_time_zone_area(area):
    workbook = openpyxl.load_workbook("adder_automation.xlsx")
    sheet1 = workbook[area]
    data = sheet1['B40'].value
    time_zone = Select(driver.find_element_by_xpath(set_timezone))
    time_zone.select_by_visible_text(data)
    print("asia is selected")

@then(parsers.parse('user set time zone location "{location}"'))
def user_set_time_zone_location(location):
    workbook = openpyxl.load_workbook("adder_automation.xlsx")
    sheet1 = workbook[location]
    data = sheet1['B41'].value
    zone_location = Select(driver.find_element_by_xpath(timezone_loc))
    zone_location.select_by_visible_text(data)
    print("kolkata is selected")

@then("user verify save done or not")
def user_verify_save_done_or_not():

    expected_message = "Settings updated"

    actual_message = driver.find_element_by_xpath(setting_update_msg).text
    if expected_message == actual_message:
        print("save message is display properly")
        print("text is :", actual_message)

        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot', attachment_type=allure.attachment_type.PNG)

    else:
        print("save message is  not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False

@then('user verify date and time change or not')
def user_verify_date_and_time_change_or_not():
    now = datetime.now()
    print("now =", now)
    # expected_date = datetime.today().strftime('%H:%M %a%e %b %Y')
    expected_date = datetime.today().strftime('%H:%M %a %#d %b %Y')
    # expected_date = datetime.today().strftime('%Y-%m-%d %H:%i:%s')
    print("date and time =", expected_date)
    actual1 = driver.find_element_by_xpath(change_date_and_time).text
    p = actual1.split(',')
    actual_date = p[0] + p[1]
    print(actual_date)
    time.sleep(10)
    driver.refresh()
    if expected_date == actual_date:
        assert True
        print("date and time is display properly")
        print("expected text is " + expected_date)
        print("actual text is " + actual_date)
        allure.attach(driver.get_screenshot_as_png(), name='screenshot', attachment_type=allure.attachment_type.PNG)
    else:
        print("date and time is not display properly")
        print("expected text is " + expected_date)
        print("actual text is " + actual_date)
        allure.attach(driver.get_screenshot_as_png(), name='screenshot', attachment_type=allure.attachment_type.PNG)
        assert False







# @then("user disable NTP option")
# def user_enable_the_SNMP():
#     driver.find_element_by_xpath('//*[@id="ntp_enabled_0"]').click()
#     print("click on NTP disable done")
#     time.sleep(2)
#
# @then("user add date day")
# def user_add_date_day():
#     date = Select(driver.find_element_by_xpath('//*[@id="date_day"]'))
#     date.select_by_visible_text("20")
#     print("date add sucessfully")
#
# @then("user add date month")
# def user_add_month():
#     month = Select(driver.find_element_by_xpath('//*[@id="date_month"]'))
#     month.select_by_visible_text("Sep")
#     print("month add sucessfully")
#
# @then(parsers.parse('user add date year "{year}"'))
# def user_add_year(year):
#     Year = driver.find_element_by_xpath('//*[@id="date_year"]').send_keys(year)
#     print("year add sucessfully")
#     time.sleep(2)
#
# @then("user add time in hour")
# def user_add_time_in_hour():
#     hour = Select(driver.find_element_by_xpath('//*[@id="time_hour"]'))
#     hour.select_by_visible_text("06")
#     print("time in hour add sucesfully")
#
# @then("user add time in minute")
# def user_add_time_in_minute():
#     minute = Select(driver.find_element_by_xpath('//*[@id="time_min"]'))
#     minute.select_by_visible_text("30")
#     print("time in minute add sucessfully")
#
# @then("user select time zone area")
# def user_select_time_zone_area():
#     time_zone = Select(driver.find_element_by_xpath('//*[@id="timezone_area"]'))
#     time_zone.select_by_visible_text("Asia")
#     print("time zone area add sucessfully")
#
# @then("user select time zone location")
# def user_select_time_zone_location():
#     location = Select(driver.find_element_by_xpath('//*[@id="timezone_location_Asia"]'))
#     location.select_by_visible_text("Kolkata")
#     print("time zone location add sucessfully")


#
# @then("user verify date and time change or not")
# def user_verify_date_and_time_change_or_not():
#
#     expected = '06:30, Mon 20 Sep 2020'
#
#     actual = driver.find_element_by_xpath("//span[@id='time_container']").text
#     real = actual[:-40]
#     print(real)
#     import pdb; pdb.set_trace()
#     # import pdb; pdb.set_trace()
#     if expected == real:
#         print("date and time is display properly")
#         print("date and time is :", actual)
#         assert True
#         allure.attach(driver.get_screenshot_as_png(), name='screenshot',
#                       attachment_type=allure.attachment_type.PNG)
#
#     else:
#         print("date and time is not display properly")
#         print("date and time is :", actual)
#         allure.attach(driver.get_screenshot_as_png(), name='screenshot',
#                       attachment_type=allure.attachment_type.PNG)
#         assert False
