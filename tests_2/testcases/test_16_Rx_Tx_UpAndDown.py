from pytest_bdd import scenario, given, when, then, scenarios, parsers
import allure
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
import time
from pagelocators.adder_common import *
from tests_1.testcases.WebDeiverFactory import driver
import openpyxl
scenarios('../features/Rx_Tx_UpAndDown.feature')



@then("user verify dashboard heading")
def user_verify_channel_heading():
    expected = 'DASHBOARD'

    actual = driver.find_element_by_xpath(dashboard).text

    if expected == actual:
        print("dashboard display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("dashboard not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False


@then("user click on dashboard")
def user_click_on_dashboard():
    driver.find_element_by_xpath(dashboard).click()
    print("clicked on dashboard")
    time.sleep(2)


@then("user verify the update option")
def user_verify_the_update_option():

    expected ="Updates"

    actual = driver.find_element_by_xpath('//*[@id="dashboard_links"]/ul/li[4]/a').text
    if expected == actual:
        print("update display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("update not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False

@then("user click on update option and verify the update page is open")
def user_click_on_update_option_and_verify_update_page():
    driver.find_element_by_xpath('//*[@id="dashboard_links"]/ul/li[4]/a').click()
    try:
        assert "updates" in driver.current_url
    finally:
        if (AssertionError):
            allure.attach(driver.get_screenshot_as_png(), name="Invalid Credential",
                          attachment_type=allure.attachment_type.PNG)
            driver.execute_script("window.scrollTo(0,500)")

@then(parsers.parse('user select image "{u_d_image}"'))
def user_select_aim_image(u_d_image):
    workbook = openpyxl.load_workbook("adder_automation.xlsx")
    sheet1 = workbook[u_d_image]
    data = sheet1['B50'].value
    driver.find_element_by_xpath('//*[@id="uploaded_firmware_file"]').send_keys(data)
    allure.attach(driver.get_screenshot_as_png(), name="Invalid Credential",
                  attachment_type=allure.attachment_type.PNG)
    time.sleep(2)


@then("user verify the upload button")
def user_verify_the_upload_button():
    expected = "Upload"

    actual = driver.find_element_by_xpath('//*[@id="admin_body"]/div[5]/a').text
    # import pdb; pdb.set_trace()
    if expected == actual:
        print("upload display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("upload not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False

@then("user click on upload button")
def user_click_on_upload_button():
    # driver.implicitly_wait(300)
    driver.find_element_by_xpath('//*[@id="admin_body"]/div[5]/a').click()
    time.sleep(2)

@then("user verify upgrade selected transmitter is shown or not")
def user_verify_upgrade_selected_transmitter_is_shown_or_not():
    expected = "Upgrade Selected Transmitters"

    actual = driver.find_element_by_xpath('//*[@id="admin_body"]/div[1]/a').text
    # import pdb; pdb.set_trace()
    if expected == actual:
        print("upgrade selected transmitter display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("upgrade selected transmitter not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False

@then("user click on upgrade checkbox")
def user_click_on_upgrade_checkbox():
    driver.find_element_by_xpath('//*[@id="select_all"]').click()
    time.sleep(2)

@then('user click on upgrade selected transmitter option')
def user_click_on_upgrade_selected_transmitter_option():
    driver.find_element_by_xpath('//*[@id="admin_body"]/div[1]/a').click()
    time.sleep(5)

@then(parsers.parse('user verify frimware upgrade or not "{rx_tx_up_down}"'))
def user_verify_upgrade_selected_transmitter_is_shown_or_not(rx_tx_up_down):
    workbook = openpyxl.load_workbook("adder_automation.xlsx")
    sheet1 = workbook[rx_tx_up_down]
    data = sheet1['B51'].value
    expected = str(data) + " device currently being upgraded with new firmware"
    # expected = "1 device currently being upgraded with new firmware"
    actual = driver.find_element_by_xpath("//span[@class='message_box mb_yellow error_message']").text
    # import pdb; pdb.set_trace()
    if expected == actual:
        print("firmware is upgrading...")
        element = WebDriverWait(driver, 300).until(
            EC.invisibility_of_element_located((By.XPATH, "//span[@class='message_box mb_yellow error_message']")))
        print("frimware upgrade")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("frimware not upgrade")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False
