from pytest_bdd import scenario, given, when, then, scenarios, parsers
import allure
import time
from pagelocators.adder_common import *
from tests_1.testcases.WebDeiverFactory import driver
import openpyxl

scenarios('../features/eth2_dhcp.feature')


@then("user verify dashboard heading")
def user_verify_channel_heading():
    expected = 'DASHBOARD'

    actual = driver.find_element_by_xpath(dashboard).text

    if expected == actual:
        print("dashboard display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("dashboard not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False


@then("user click on dashboard")
def user_click_on_dashboard():
    driver.find_element_by_xpath(dashboard).click()
    print("clicked on dashboard")
    time.sleep(2)

@then("user verify setting option")
def user_verify_setting_option():

    expected = 'Settings'

    actual = driver.find_element_by_xpath('//*[@id="dashboard_links"]/ul/li[2]/a').text

    if expected == actual:
        print("setting display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("setting is not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False

@then("user click on setting option and verify setting page is open or not")
def user_click_on_setting_option_and_verify_setting_page_is_open_or_not():

    driver.find_element_by_xpath('//*[@id="dashboard_links"]/ul/li[2]/a').click()
    print("clicked on setting")
    time.sleep(2)
    try:
        assert "setting" in driver.current_url
    finally:
        if (AssertionError):
            allure.attach(driver.get_screenshot_as_png(), name="Invalid Credential",
                          attachment_type=allure.attachment_type.PNG)

@then("user verify network option")
def user_verify_network_option():

    expected = 'Network'

    actual = driver.find_element_by_xpath('//*[@id="tabs"]/li[5]/a').text

    if expected == actual:
        print("network display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("network is not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False

@then("user click on network option")
def user_click_on_network_option():
    driver.find_element_by_xpath('//*[@id="tabs"]/li[5]/a').click()
    print("clicked on network option")
    time.sleep(2)
    driver.execute_script("window.scrollTo(0,400)")

@then("user clear the gateway ip")
def user_clear_the_gateway_ip():
    driver.find_element_by_xpath('//*[@id="ip_gateway"]').clear()
    print("gateway ip clear")
    time.sleep(1)

@then("user click on DHCP checkbox")
def user_click_on_DHCP_checkbox():
    driver.find_element_by_xpath('//*[@id="eth1_enabled_1"]').click()
    driver.execute_script("window.scrollTo(0,1000)")


@then("user verify the save button")
def user_verify_the_save_button():
    expected = "Save"

    actual = driver.find_element_by_xpath('//*[@id="save_button"]').text

    if expected == actual:
        print("save display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("save is not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False

@then("user save the details")
def user_save_the_details():
    driver.find_element_by_xpath('//*[@id="save_button"]').click()
    time.sleep(2)


@then("user verify save done or not")
def user_verify_save_done_or_not():

    expected_message = "Settings updated"

    actual_message = driver.find_element_by_xpath("//span[@class='message_box mb_green error_message']").text
    if expected_message == actual_message:
        print("save message is display properly")
        print("text is :", actual_message)

        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot', attachment_type=allure.attachment_type.PNG)
        time.sleep(65)
        driver.refresh()
    else:
        print("save message is  not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False

@then("user get the ip genrate by DHCP")
def user_get_the_ip_genrate_by_DHCP():
    ip = driver.find_element_by_xpath('//*[@id="eth1_dhcp_div"]/div[2]/div[2]').text
    print(ip)
    allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                  attachment_type=allure.attachment_type.PNG)

@then(parsers.parse('user verify the ip by login "{id}"'))
def user_should_enter_username(id):
    ip = driver.find_element_by_xpath('//*[@id="eth1_dhcp_div"]/div[2]/div[2]').text
    try:
        driver.execute_script("window.open('about:blank',"
                              "'secondtab'); ")
        driver.switch_to.window("secondtab")
        time.sleep(2)
        driver.get("http:"+ip)
        time.sleep(2)
        workbook = openpyxl.load_workbook("adder_automation.xlsx")
        sheet1 = workbook[id]
        data = sheet1['B5'].value
        email = driver.find_element_by_xpath(username).clear()
        email = driver.find_element_by_xpath(username).send_keys(data)
        time.sleep(2)
        driver.find_element_by_xpath(login_button).click()
        time.sleep(2)
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        time.sleep(2)
        driver.close()
        driver.switch_to.window(window_name= driver.window_handles[0])
        time.sleep(2)
    finally:
        if (AssertionError):
            allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                          attachment_type=allure.attachment_type.PNG)
            # driver.close()
            driver.switch_to.window(window_name=driver.window_handles[0])


