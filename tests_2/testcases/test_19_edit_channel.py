from pytest_bdd import given, then, scenarios, parsers
import allure
import time
from selenium.webdriver.support.select import Select
from pagelocators.adder_common import *
from pagelocators.testcase2 import *
from tests_1.testcases.WebDeiverFactory import driver
import openpyxl


scenarios('../features/edit_channel.feature')

@then("user verify channel heading")
def user_verify_channel_heading():

    expected = 'CHANNELS'

    actual = driver.find_element_by_xpath(channel).text

    if expected == actual:
        print("channel display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("channel not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False


@then("user click on channel and verify the channel page is open or not")
def user_click_on_channel_and_verify_the_channel_page_is_open_or_not():
    driver.find_element_by_xpath(channel).click()
    print("clicked on channel")
    time.sleep(2)
    try:
        assert "channels" in driver.current_url
    finally:
        if (AssertionError):
            allure.attach(driver.get_screenshot_as_png(), name="Invalid Credential",
                          attachment_type=allure.attachment_type.PNG)

@then("user click on the edit channel")
def user_click_on_the_add_channel_option():
    workbook = openpyxl.load_workbook("adder_automation.xlsx")
    sheet1 = workbook['adder']
    data = sheet1['B11'].value
    driver.find_element_by_xpath(f"//td[text()='{data}']/../td[9]/a[1]").click()
    print("click on channel done")
    time.sleep(1)

@then(parsers.parse('user add channel name "{c_name}"'))
def user_add_channel_name(c_name):
    workbook = openpyxl.load_workbook("adder_automation.xlsx")
    sheet1 = workbook[c_name]
    data = sheet1['B12'].value
    driver.find_element_by_xpath(channel_name).clear()
    driver.find_element_by_xpath(channel_name).send_keys(data)
    print("channel name aadded")
    time.sleep(1)

@then(parsers.parse('user add description "{c_description}"'))
def user_add_description(c_description):
    workbook = openpyxl.load_workbook("adder_automation.xlsx")
    sheet1 = workbook[c_description]
    data = sheet1['B13'].value
    driver.find_element_by_xpath(channel_description).clear()
    driver.find_element_by_xpath(channel_description).send_keys(data)
    print("channel description added")
    time.sleep(1)

@then(parsers.parse('user add location "{c_location}"'))
def user_add_location(c_location):
    workbook = openpyxl.load_workbook("adder_automation.xlsx")
    sheet1 = workbook[c_location]
    data = sheet1['B14'].value
    driver.find_element_by_xpath(channel_location).clear()
    driver.find_element_by_xpath(channel_location).send_keys(data)
    print("channel location added")
    time.sleep(1)

@then("user select video1")
def user_select_video1():
    workbook = openpyxl.load_workbook("adder_automation.xlsx")
    sheet1 = workbook['adder']
    data = sheet1['B15'].value
    video1 = Select(driver.find_element_by_xpath(video_1))
    video1.select_by_visible_text(data)
    print("video1 added")
    time.sleep(1)

# @then("user select video2")
# def user_select_video2():
#     workbook = openpyxl.load_workbook("adder_automation.xlsx")
#     sheet1 = workbook['adder']
#     data = sheet1['B16'].value
#     video2 = Select(driver.find_element_by_xpath(video_2))
#     video2.select_by_visible_text(data)
#     print("video2 added")
#     time.sleep(3)

@then("user select audio")
def user_select_audio():
    workbook = openpyxl.load_workbook("adder_automation.xlsx")
    sheet1 = workbook['adder']
    data = sheet1['B17'].value
    Audio = Select(driver.find_element_by_xpath(audio))
    Audio.select_by_visible_text(data)
    print("audio added")
    time.sleep(1)

@then("user select usb")
def user_select_usb():
    workbook = openpyxl.load_workbook("adder_automation.xlsx")
    sheet1 = workbook['adder']
    data = sheet1['B18'].value
    Usb = Select(driver.find_element_by_xpath(usb))
    Usb.select_by_visible_text(data)
    print("usb added")
    allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                  attachment_type=allure.attachment_type.PNG)


@then("user send the channel group not a member of to member of")
def user_send_the_channel_group_not_a_member_of_to_member_of():
    driver.find_element_by_xpath(p_channel_group).click()
    print("channel group added")
    time.sleep(2)
    driver.execute_script("window.scrollTo(0,500)")
    time.sleep(2)

@then("user allow the permission of single user")
def user_allow_the_permission_of_single_user():
    single_user = Select(driver.find_element_by_xpath(p_single_user))
    single_user.select_by_visible_text("admin")
    driver.find_element_by_xpath(b_single_user).click()
    print("user selected")
    time.sleep(1)
    allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                  attachment_type=allure.attachment_type.PNG)

@then("user disselect the permission of single user")
def user_disselect_the_permission_of_single_user():
    disselect = Select(driver.find_element_by_xpath(disselect_single_user))
    disselect.select_by_visible_text("admin")
    driver.find_element_by_xpath(b_disselect_single_user).click()
    print("user disselected")
    time.sleep(1)
    allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                  attachment_type=allure.attachment_type.PNG)

@then('user allow the permission of all the user')
def user_allow_the_permission_of_all_the_user():
    driver.find_element_by_xpath(p_all_user).click()
    print("all user selected")
    time.sleep(2)
    allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                  attachment_type=allure.attachment_type.PNG)

@then("user selected the user group permission not set to permitted")
def user_selected_the_user_group_permission_not_set_to_permitted():
    driver.find_element_by_xpath(group_permission).click()
    time.sleep(2)
    allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                  attachment_type=allure.attachment_type.PNG)


@then("user verify the save button")
def user_verify_the_save_button():
    expected = "Save"

    actual = driver.find_element_by_xpath(save).text

    if expected == actual:
        print("save display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("save is not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False

@then("user save the details")
def user_save_the_details():
    driver.find_element_by_xpath(save).click()
    time.sleep(3)


# @then("user verify same channel is created or not")
# def user_verify_same_channel_is_created_or_not():
#     expected = "A channel with that name already exists"
#
#     actual = driver.find_element_by_id('configure_channel_ajax_message').text
#     if expected == actual:
#         print("A channel with that name already exists")
#         allure.attach(driver.get_screenshot_as_png(), name='screenshot',
#                       attachment_type=allure.attachment_type.PNG)
#
#         driver.execute_script("window.scrollTo(500,0)")
#         assert False
#
#     else:
#         print("channel is created")
#         assert True
#         allure.attach(driver.get_screenshot_as_png(), name='screenshot',
#                       attachment_type=allure.attachment_type.PNG)
#         driver.execute_script("window.scrollTo(500,0)")



############ channel connect #####################

@then("user verify receiver heading")
def user_verify_reciver_heading():

    expected = 'RECEIVERS'

    actual = driver.find_element_by_xpath(receiver).text

    if expected == actual:
        print("channel display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("channel not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False


@then("user click on receiver and verify the receiver page is open or not")
def user_click_on_channel_and_verify_the_channel_page_is_open_or_not():
    driver.find_element_by_xpath(receiver).click()
    print("clicked on receiver")
    time.sleep(2)
    try:
        assert "t=rx" in driver.current_url
        print("we are on receiver page")
    finally:
        if (AssertionError):
            allure.attach(driver.get_screenshot_as_png(), name="Invalid Credential",
                          attachment_type=allure.attachment_type.PNG)


@then("user click on connect to a channel")
def user_click_on_connect_to_a_channel():
    # driver.find_element_by_xpath('//*[@id="connect_link_101"]/a').click()
    driver.find_element_by_xpath(connect_channel).click()
    time.sleep(2)


# @then("user verify video-only button is present or not")
# def user_verify_video_button_is_present_or_not():
#
#     expected = "VIDEO-ONLY"
#
#     actual = driver.find_element_by_xpath(video_only_button).text
#     # import pdb; pdb.set_trace()
#     if expected == actual:
#         print("video-only is display properly")
#         assert True
#         allure.attach(driver.get_screenshot_as_png(), name='screenshot',
#                       attachment_type=allure.attachment_type.PNG)
#
#     else:
#         print("video-only is not display properly")
#         allure.attach(driver.get_screenshot_as_png(), name='screenshot',
#                       attachment_type=allure.attachment_type.PNG)
#         assert False

@then(parsers.parse('user click on "{mode}"'))
def user_click_on(mode):
    workbook = openpyxl.load_workbook("adder_automation.xlsx")
    sheet1 = workbook[mode]
    data = sheet1['B19'].value
    name1 = sheet1['B12'].value
    print(name1)
    mode1 = "Video-only"
    mode2 = "Shared"
    mode3 = "Exclusive"
    mode4 = "Private"
    if (mode1 == data):
        x = 1
        driver.find_element_by_xpath(f"//a[text()='{name1}']/../../td[4]/a{[x]}").click()
        print("click on video-only option")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        time.sleep(2)

    elif (mode2 == data):
        x = 2
        driver.find_element_by_xpath(f"//a[text()='{name1}']/../../td[4]/a{[x]}").click()
        print("click on shared option")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        time.sleep(2)

    elif (mode3 == data):
        x = 3
        driver.find_element_by_xpath(f"//a[text()='{name1}']/../../td[4]/a{[x]}").click()
        print("click on exclusive option")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        time.sleep(2)

    elif (mode4 == data):
        x = 4
        driver.find_element_by_xpath(f"//a[text()='{name1}']/../../td[4]/a{[x]}").click()
        print("click on private option")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        time.sleep(2)

    else:
        print("preset channel in not working properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False


@then("user verify the channel is connect or not")
def user_verify_the_channel_is_connect_or_not(c_name):
    # import pdb;pdb.set_trace()
    workbook = openpyxl.load_workbook("adder_automation.xlsx")
    sheet1 = workbook[c_name]
    data = sheet1['B12'].value

    expected = "This receiver is currently connected to " + data
    actual_message = driver.find_element_by_xpath(v_channel_connect_msg).text
    real = actual_message[:-41]
    print(real)
    # import pdb; pdb.set_trace()
    if expected == real:
        print("channel is connected display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("channel is not connected")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False


@then("user verify active connection heading")
def user_verify_active_connection_heading():

    expected = 'Active Connections'

    actual = driver.find_element_by_xpath(active_connection).text

    if expected == actual:
        print("Active Connection tab display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("Active Connections tab not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False


@then("user click on active connection tab")
def user_click_on_active_connection_tab():
    driver.find_element_by_xpath(active_connection).click()
    print("clicked on active connection tab")
    time.sleep(3)



@then("user verify the channel name is same which we created or not in active connection")
def user_verify_the_channel_name_is_same_which_we_created_or_not_in_active_connection(c_name):
    workbook = openpyxl.load_workbook("adder_automation.xlsx")
    sheet1 = workbook[c_name]
    data = sheet1['B12'].value
    expected = data

    actual = driver.find_element_by_xpath(v_channel_name).text
    if expected == actual:
        print("same channel name is display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("same channel name is not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False


@then("user verify channel is connect with same reciever or not in active connection")
def user_verify_channel_is_connect_with_same_reciever_or_not_in_active_connection(c_name):
    driver.find_element_by_xpath(v_reciver_name_active_connection).click()
    workbook = openpyxl.load_workbook("adder_automation.xlsx")
    sheet1 = workbook[c_name]
    data = sheet1['B12'].value
    # import pdb;pdb.set_trace()
    expected = "This receiver is currently connected to " + data

    actual = driver.find_element_by_xpath(v_channel_connect_msg).text
    real = actual[:-41]
    print(real)
    # import pdb; pdb.set_trace()
    if expected == real:
        print("channel is connected with same receiver is display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("channel is not connected with same receiver")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False




@then("user verify connection log heading")
def user_verify_connection_log_heading():

    expected = 'Connection Log'

    actual = driver.find_element_by_xpath(connection_log).text

    if expected == actual:
        print("Connection Log tab display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("Connection Log tab not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False


@then("user click on connection log tab")
def user_click_on_connection_log_tab():
    driver.find_element_by_xpath(connection_log).click()
    print("clicked on Connection Log tab")
    time.sleep(5)



@then("user verify the channel name is same which we created or not in connection log")
def user_verify_the_channel_name_is_same_which_we_created_or_not_in_connection_log(c_name):
    workbook = openpyxl.load_workbook("adder_automation.xlsx")
    sheet1 = workbook[c_name]
    data = sheet1['B12'].value
    expected = data

    actual = driver.find_element_by_xpath(v_cname_connection_log).text

    if expected == actual:
        print("same channel name is display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("same channel name is not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False


@then("user verify channel is connect with same reciever or not in connection log")
def user_verify_channel_is_connect_with_same_reciever_or_not_in_connection_log(c_name):
    driver.find_element_by_xpath(v_reciver_name_connection_log).click()
    # import pdb;pdb.set_trace()
    workbook = openpyxl.load_workbook("adder_automation.xlsx")
    sheet1 = workbook[c_name]
    data = sheet1['B12'].value
    expected = "This receiver is currently connected to " + data

    actual = driver.find_element_by_xpath(v_channel_connect_msg).text
    real = actual[:-41]
    print(real)
    # import pdb; pdb.set_trace()
    if expected == real:
        print("channel is connected with same receiver is display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("channel is not connected with same receiver")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False



@then("user click on channel disconnect")
def user_click_on_channel_disselect():
    # driver.find_element_by_xpath('//*[@id="disconnect_link_101"]/a').click()
    driver.find_element_by_xpath(channel_disconnect).click()
    print("channel is disconnect")
    allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                  attachment_type=allure.attachment_type.PNG)
    time.sleep(5)


@then("user delete the channel which is created")
def user_delete_the_channel_which_is_created():
    driver.find_element_by_xpath('//*[@id="row_id_3947501"]/td[9]/a[3]/img').click()
    print("channel is deleted")
    time.sleep(3)


@then("user verify delete option is occur or not")
def user_verify_delete_option_is_occur_or_not():
    expected = "Delete"

    actual = driver.find_element_by_xpath(final_delete).text

    if expected == actual:
        print("delete option is display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("delete option is not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False

@then("user click on the delete option")
def user_click_on_the_delete_option():
    driver.find_element_by_xpath(final_delete).click()
    print("click on final delete")
    allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                  attachment_type=allure.attachment_type.PNG)
    time.sleep(3)


@then("user verify dashboard heading")
def user_verify_channel_heading():
    expected = 'DASHBOARD'

    actual = driver.find_element_by_xpath(dashboard).text

    if expected == actual:
        print("dashboard display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("dashboard not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False


@then("user click on dashboard")
def user_click_on_dashboard():
    driver.find_element_by_xpath(dashboard).click()
    print("clicked on dashboard")
    time.sleep(5)
    allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                  attachment_type=allure.attachment_type.PNG)
