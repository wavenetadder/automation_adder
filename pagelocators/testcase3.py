add_preset = "//div/a[@href='configure_connection_preset.php']"

preset_name = '//*[@id="cp_name"]'

preset_description = '//*[@id="cp_description"]'

receiver_pair = "//select[starts-with(@id, 'receiver_')]"

channel_pair = "//select[starts-with(@id, 'channel_')]"

allowed_connection = '//*[@id="inherit_allowed_modes_1"]'

preset_video_only = "//tr[starts-with(@id, 'row_id_')]/td[5]/a[1]"

preset_channel_connect = '//*[@id="admin_body"]/div[1]'

preset_active_connection = '//*[@id="dashboard_links"]/ul/li[5]/a'

v_preset_channel_name = '//*[@id="admin_body"]/table/tbody/tr/td[4]/a'

v_preset_name = '//*[@id="admin_body"]/table/tbody/tr/td[5]/a'

v_preset_reciver_name_active_connection = '//*[@id="admin_body"]/table/tbody/tr/td[3]/span/a'

v_preset_msg = "//div[@class='mb_yellow message_box']"

preset_connection_log = '//*[@id="dashboard_links"]/ul/li[6]/a'

v_preset_cname_connection_log = '//*[@id="admin_body"]/table/tbody/tr[1]/td[5]/a'

v_preset_name_connection_log = '//*[@id="admin_body"]/table/tbody/tr[1]/td[6]/a'

v_preset_reciver_name_connection_log = '//*[@id="admin_body"]/table/tbody/tr[1]/td[4]/span/a'

preset_channel_disconnect = "//tr[starts-with(@id, 'row_id_')]/td[5]/a[1]"

preset_shared = "//tr[starts-with(@id, 'row_id_')]/td[5]/a[2]"

preset_exclusive = "//tr[starts-with(@id, 'row_id_')]/td[5]/a[3]"

preset_private = "//tr[starts-with(@id, 'row_id_')]/td[5]/a[4]"

# delete_preset = "//img[@alt='Delete preset']"

preset_final_delete = '//*[@id="delete_connection_preset_confirm_link"]'