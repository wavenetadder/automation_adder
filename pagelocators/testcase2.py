update = '//*[@id="dashboard_links"]/ul/li[4]/a'

choose_file = '//*[@id="uploaded_aim_upgrade_file"]'
# choose_file = 'uploaded_aim_upgrade_file'   #id

upload_button = '//*[@id="admin_body"]/div[1]/span/a'

upload_msg = "//span[@class='message_box mb_green error_message']"

update_buildNo_verify = '//*[@id="admin_body"]/div[2]/div[2]/ul/li[2]/label'

update_restart_button = '//*[@id="admin_body"]/div[1]/a'

# build_no = "//label[starts-with(@for, 'reset_image_upgrade')]"

update_buildNo_end = '//*[@id="footer_version"]'

add_channel = '//*[@id="channels_links"]/ul/li[2]/a'

channel_name = '//*[@id="c_name"]'

channel_description = '//*[@id="c_description"]'

channel_location = '//*[@id="c_location"]'

video_1 = '//*[@id="video_e_id"]'

video_2 = '//*[@id="video1_e_id"]'

audio = '//*[@id="audio_e_id"]'

usb = '//*[@id="usb_e_id"]'

p_channel_group = '//*[@id="add_all_channel_groups"]/img'

p_single_user = '//*[@id="all_users"]'

b_single_user = '//*[@id="add_one_user"]/img'

disselect_single_user = '//*[@id="selected_users"]'

b_disselect_single_user = '//*[@id="remove_one_user"]/img'

p_all_user = '//*[@id="add_all_users"]/img'

group_permission = '//*[@id="add_all_user_groups"]/img'

connect_channel = "//span[starts-with(@id, 'connect_link_')]"

video_only_button = '//*[@id="admin_body"]/table/tbody/tr[2]/td[4]/a[1]'

v_channel_connect_msg = "//div[@class='mb_yellow message_box']"

active_connection = '//*[@id="dashboard_links"]/ul/li[5]/a'

v_channel_name = '//*[@id="admin_body"]/table/tbody/tr/td[4]/a'

v_reciver_name_active_connection = '//*[@id="admin_body"]/table/tbody/tr/td[3]/span/a'

# active_connection_msg = "//div[@class='mb_yellow message_box']"

connection_log = '//*[@id="dashboard_links"]/ul/li[6]/a'

v_cname_connection_log = '//*[@id="admin_body"]/table/tbody/tr[1]/td[5]/a'

v_reciver_name_connection_log = '//*[@id="admin_body"]/table/tbody/tr[1]/td[4]/span/a'

channel_disconnect = "//span[starts-with(@id, 'disconnect_link_')]"

shared = '//*[@id="admin_body"]/table/tbody/tr[2]/td[4]/a[2]'

exclusive = '//*[@id="admin_body"]/table/tbody/tr[2]/td[4]/a[3]'

private = '//*[@id="admin_body"]/table/tbody/tr[2]/td[4]/a[4]'

delete_channel = "//tr[starts-with(@class, 'odd')]/td[9]/a[3]/img"

# occur_delete = '//*[@id="delete_channel_confirm_link"]'

final_delete = '//*[@id="delete_channel_confirm_link"]'
