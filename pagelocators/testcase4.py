setting = '//*[@id="dashboard_links"]/ul/li[2]/a'

network = '//*[@id="tabs"]/li[5]/a'

network_option = '//*[@id="tabs"]/li[5]/a'

static_checkbox = '//*[@id="eth1_enabled_2"]'

aim_ip_address = '//*[@id="ip_aim_server1"]'

aim_netmask = '//*[@id="ip_netmask1"]'

dns_server = '//*[@id="ip_name_server1"]'

aim_gateway = '//*[@id="ip_gateway"]'

time1 = '//*[@id="tab_time"]'

setting_update_msg = "//span[@class='message_box mb_green error_message']"

ntp_enable = '//*[@id="ntp_enabled_1"]'

ntp_server = '//*[@id="ntp_hide_row_1"]/div[2]/a'

nto_serverone_address = '//*[@id="ntp_server_1"]'

set_timezone = '//*[@id="timezone_area"]'

timezone_loc = '//*[@id="timezone_location_Asia"]'

change_date_and_time = "//span[@id='time_container']"