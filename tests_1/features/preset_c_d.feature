@preset @alltestcases
Feature: AIM functionality of preset page

@preset
Scenario: Verify the functionality of preset page and connect preset
  Then user verify preset heading
  Then user click on preset and verify the preset page is open or not
  Then user verify add preset option is present or not
  Then user click on add preset option
  When user enter preset name "adder"
  Then user enter description "adder"
  Then user add channel pair 1 receiver
  Then user add channel pair 1 channel
  Then user choose allowed conection mode
  Then user verify the save button
  Then user save the details

####connect the preset channel with video-only,shared,exclusive,private####

  Given user click on "adder"
  Then user verify receiver heading on top
  Then user click on receiver and verify the receiver page is open or not
  Then user click on connect to a channel
  Given user verify the channel is connect "adder"

  Then user verify dashboard heading
  Then user click on dashboard
  Then user verify active connection heading
  Then user click on active connection tab
  Then user verify the channel name is same which we created or not in active connection
  Then user verify preset name is same which we created or not in active connection
  Then user verify channel is connect with same reciever or not in active connection

  Then user verify dashboard heading
  Then user click on dashboard
  Then user verify connection log heading
  Then user click on connection log tab
  Then user verify the channel name is same which we created or not in connection log
  Then user verify preset name is same which we created or not in connection log
  Then user verify channel is connect with same reciever or not in connection log

  Then user verify preset heading
  Then user click on preset and verify the preset page is open or not
  Then user click on channel disconnect

  Then user delete the preset
  Then user verify delete option come or not
  Then user click on delete option
  Then user click on dashboard
