@stats @alltestcases
Feature: we are working on Aim and check the statistics page

@stats
Scenario: user verify and click the dashboard and check the statistics page
  Then user verify dashboard heading
  Then user click on dashboard
  Then user verify statistics option
  Then user click on statistics option and verify statistics page is open or not
  Then user click on enable all devices
  Then user verify the tx name on statistics page "adder"
  Then user click on tx option and verify tx statistics page is open or not
  Then user verify the rx name on statistics page "adder"
  Then user click on rx option and verify rx statistics page is open or not
  Then user click on dashboard