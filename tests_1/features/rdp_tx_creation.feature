@rdp @alltestcases
Feature: we are working on Aim box and doing RDP_TX creation

######### RDP Creation ###################

@rdp
Scenario: user verify and click the transmitter and perform opertaion

  Then user verify transmitter heading
  Then user click on transmitter and verify the transmitter page is open or not
  Then user verify Add VDI option
  Then user click on Add VDI option
  Then user enter RDP name "adder"
  Then user enter ip address and DNS name "adder"
  Then user enter port number "adder"
  Then user enter domain name "adder"
  Then user select maximum resolution
  Then user verify the save button
  Then user save the details

########### RDP Deletion ################

#Scenario: user go to transmitter page
#  Then user click on transmitter and verify the transmitter page is open or not
##  Then user delete the rdp which is created
##  Then user verify delete option come or not
##  Then user click on delete option
#  Then user verify dashboard heading
#  Then user click on dashboard
