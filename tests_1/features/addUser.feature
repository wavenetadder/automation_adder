@user1 @alltestcases
Feature: AIM : Functionality create user

########## add user ################
@user1
Scenario: create user with admin permission
  Then user verify user heading
  Then user click on user and verify the user page is open or not
  Then user verify add user button
  Then user click on add user button
  Then user add username "adder"
  Then user add first name "adder"
  Then user add last name "adder"
  Then user add email "adder"
  Then user click on checkbox required password no
  Then user click on checkbox aim admin required yes
  Then user give permission of user group not a member of to member of
  Then user give permission of channel permission not set to permitted
  Then user give permission channel group permission not set to permitted
  Then user verify the save button
  Then user click on save button

  Then user click on logout
  Then user enter the username after add user
  Then user click on login button
  Then user verify the login done or not
  Then user click on logout

#### agin login with main account ####
  Given user is in aim login page or not
  Given user enter the username "adder"
#  Given user enter the password "123"
  Then user click on login button
  Then user verify the login done or not
  Then user verify user heading
  Then user click on user and verify the user page is open or not
  Then user verify dashboard heading
  Then user click on dashboard
#  Then delete the created user








############# user is already present or not ##################

#Scenario: Again create user and verify already present or not
#  Then user verify user heading
#  Then user click on user and verify the user page is open or not
#  Then user verify add user button
#  Then user click on add user button
#  Then user add username "admin123"
#  Then user add first name "pankaj"
#  Then user add last name "kumar"
#  Then user add email "pankajk0692@gmail.com"
#  Then user click on checkbox required password no
#  Then user click on checkbox aim admin required yes
#  Then user give permission of user group not a member of to member of
#  Then user give permission of channel permission not set to permitted
#  Then user give permission channel group permission not set to permitted
#  Then user verify the save button
#  Then user click on save button
#  Then user verify same user is created or not


###### user login with new user #############







#//img[@alt='Delete User']