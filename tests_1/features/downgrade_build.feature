@downgrade @alltestcases
Feature: we are working on Aim box and downgrade build

@downgrade
Scenario: Verify the functionality of downgrade manager
  Then user verify dashboard heading
  Then user click on dashboard
  Then user verify the update option
  Then user click on update option and verify the update page is open
  Then select aim image "adder"
  Then user verify the upload button
  Then user click on upload button
  Then user verify aim image is upload or not
  Then user verify restart button
  Then user click on restart button and land on login page

###verify the aim build is upgrade or not####

  Given user enter the username "adder"
  Then user click on login button
  Then user verify the login done or not
  Then user verify dashboard heading
  Then user click on dashboard
  Then user verify the update option
  Then user click on update option and verify the update page is open
  Then user verify the build "adder"
  Then user verify the build number in bottom of page
  Then user click on dashboard
