@txreboot @alltestcases
Feature: Aim: Reboot transmitter

@txreboot
Scenario: reboot the Aim devices : Tx
  Then user verify transmitter heading
  Then user click on transmitter and verify the transmitter page is open or not
  Then user click on the reboot option
  Then user verify reboot button is appear or not
  Then user click on reboot button
  Then user verify dashboard heading
  Then user click on dashboard
#  Then user verify tx reboot done or not