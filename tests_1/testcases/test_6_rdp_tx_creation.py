import openpyxl
from pytest_bdd import scenario, given, when, then, scenarios, parsers
import allure
from selenium.webdriver.support.select import Select

from pagelocators.adder_common import *
from tests_1.testcases.WebDeiverFactory import driver
import time

scenarios('../features/rdp_tx_creation.feature')

@then("user verify transmitter heading")
def user_verify_channel_heading():
    expected = 'TRANSMITTERS'

    actual = driver.find_element_by_xpath(transmitter).text

    if expected == actual:
        print("transmitter display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("transmitter not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False


@then("user click on transmitter and verify the transmitter page is open or not")
def user_click_on_transmitter_and_verify_the_transmitter_page_is_open_or_not():
    driver.find_element_by_xpath(transmitter).click()
    print("clicked on transmitter")
    time.sleep(2)
    try:
        assert "t=tx" in driver.current_url
    finally:
        if (AssertionError):
            allure.attach(driver.get_screenshot_as_png(), name="Invalid Credential",
                          attachment_type=allure.attachment_type.PNG)


@then("user verify Add VDI option")
def user_verify_Add_VDI_option():
    expected = 'Add VDI'

    actual = driver.find_element_by_xpath('//*[@id="transmitters_links"]/ul/li[2]/a').text

    if expected == actual:
        print("Add VDI is display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("Add VDI is not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False

@then("user click on Add VDI option")
def user_click_on_Add_VDI_option():
    driver.find_element_by_xpath('//*[@id="transmitters_links"]/ul/li[2]/a').click()
    time.sleep(2)

@then(parsers.parse('user enter RDP name "{rdp_name}"'))
def user_enter_NMS_address(rdp_name):
    workbook = openpyxl.load_workbook("adder_automation.xlsx")
    sheet1 = workbook[rdp_name]
    data = sheet1['B24'].value
    driver.find_element_by_xpath('//*[@id="r_name"]').clear()
    driver.find_element_by_xpath('//*[@id="r_name"]').send_keys(data)
    time.sleep(2)

@then(parsers.parse('user enter ip address and DNS name "{dns_name}"'))
def user_enter_ip_address_and_DNS_name(dns_name):
    workbook = openpyxl.load_workbook("adder_automation.xlsx")
    sheet1 = workbook[dns_name]
    data = sheet1['B25'].value
    driver.find_element_by_xpath('//*[@id="r_ip"]').clear()
    driver.find_element_by_xpath('//*[@id="r_ip"]').send_keys(data)
    time.sleep(2)

@then(parsers.parse('user enter port number "{port_no}"'))
def user_enter_port_number(port_no):
    workbook = openpyxl.load_workbook("adder_automation.xlsx")
    sheet1 = workbook[port_no]
    data = sheet1['B26'].value
    driver.find_element_by_xpath('//*[@id="r_port"]').clear()
    driver.find_element_by_xpath('//*[@id="r_port"]').send_keys(data)
    time.sleep(2)

@then(parsers.parse('user enter domain name "{domain_name}"'))
def user_enter_domain_name(domain_name):
    workbook = openpyxl.load_workbook("adder_automation.xlsx")
    sheet1 = workbook[domain_name]
    data = sheet1['B27'].value
    driver.find_element_by_xpath('//*[@id="r_domain"]').clear()

    driver.find_element_by_xpath('//*[@id="r_domain"]').send_keys(data)

@then("user select maximum resolution")
def user_select_maximum_resolution():
    resolution = Select(driver.find_element_by_xpath('//*[@id="r_resolution"]'))
    resolution.select_by_visible_text('USE GLOBAL SETTINGS')
    time.sleep(1)

@then("user verify the save button")
def user_verify_the_save_button():
    expected = "Save"

    actual = driver.find_element_by_xpath('//*[@id="save_button"]').text

    if expected == actual:
        print("save display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        time.sleep(2)

    else:
        print("save is not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False

@then("user save the details")
def user_save_the_details_and_land_to_transmitter():
    driver.find_element_by_xpath('//*[@id="save_button"]').click()
    print("click on save button")

# @then("user verify same RDP already reated or not")
# def user_verify_same_RDP_already_reated_or_not():
#     expected = "RDP Name already in use"
#
#     actual = driver.find_element_by_id('configure_channel_ajax_message').text
#     if expected == actual:
#         print("RDP name already in use")
#         allure.attach(driver.get_screenshot_as_png(), name='screenshot',
#                       attachment_type=allure.attachment_type.PNG)
#         assert False
#
#     else:
#         print("RDP is created")
#         assert True
#         allure.attach(driver.get_screenshot_as_png(), name='screenshot',
#                       attachment_type=allure.attachment_type.PNG)


@then("user delete the rdp which is created")
def user_delete_the_rdp_which_is_created():
    driver.find_element_by_xpath("//tr[starts-with(@class, 'even')]/td[11]/a/img").click()
    print("click on rdp delete option")
    time.sleep(1)

@then("user verify delete option come or not")
def user_verify_delete_option_come_or_not():
    expected = "Delete"

    actual = driver.find_element_by_xpath('//*[@id="delete_device_confirm_link"]').text

    if expected == actual:
        print("delete option is display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("delete option is not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False

@then("user click on delete option")
def user_click_on_delete_option():
    driver.find_element_by_xpath('//*[@id="delete_device_confirm_link"]').click()
    print("click on final delete")
    time.sleep(1)
    allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                  attachment_type=allure.attachment_type.PNG)

@then("user verify dashboard heading")
def user_verify_channel_heading():
    expected = 'DASHBOARD'

    actual = driver.find_element_by_xpath(dashboard).text

    if expected == actual:
        print("dashboard display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("dashboard not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False


@then("user click on dashboard")
def user_click_on_dashboard():
    driver.find_element_by_xpath(dashboard).click()
    print("clicked on dashboard")
    time.sleep(5)
