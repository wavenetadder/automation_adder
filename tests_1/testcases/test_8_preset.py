import openpyxl
from pytest_bdd import given, when, then, scenarios, parsers
import allure
import time
from selenium.webdriver.support.select import Select
from pagelocators.adder_common import *
from pagelocators.testcase3 import *
from tests_1.testcases.WebDeiverFactory import driver

scenarios('../features/preset_c_d.feature')

@then("user verify preset heading")
def user_verify_channel_heading():
    expected = 'PRESETS'

    actual = driver.find_element_by_xpath(preset).text

    if expected == actual:
        print("preset display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("preset not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False


@then("user click on preset and verify the preset page is open or not")
def user_click_on_preset_and_verify_the_preset_page_is_open_or_not():
    driver.find_element_by_xpath(preset).click()
    print("clicked on preset")
    time.sleep(2)
    try:
        assert "presets" in driver.current_url
    finally:
        if (AssertionError):
            allure.attach(driver.get_screenshot_as_png(), name="Invalid Credential",
                          attachment_type=allure.attachment_type.PNG)

@then("user verify add preset option is present or not")
def user_verify_add_preset_option_is_present_or_not():
    expected = 'Add Preset'

    actual = driver.find_element_by_xpath(add_preset).text
    if expected == actual:
        print("Add preset is display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("Add preset is not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False

@then('user click on add preset option')
def user_click_on_add_preset_option():
    driver.find_element_by_xpath(add_preset).click()
    print("click done on add preset")

@when(parsers.parse('user enter preset name "{preset_name1}"'))
def user_enter_preset_name(preset_name1):
    workbook = openpyxl.load_workbook("adder_automation.xlsx")
    sheet1 = workbook[preset_name1]
    data = sheet1['B30'].value
    driver.find_element_by_xpath('//*[@id="cp_name"]').clear()
    time.sleep(1)
    driver.find_element_by_xpath('//*[@id="cp_name"]').send_keys(data)
    print("preset name enter done")
    time.sleep(2)


@then(parsers.parse('user enter description "{preset_desc}"'))
def user_enter_preset_name(preset_desc):
    workbook = openpyxl.load_workbook("adder_automation.xlsx")
    sheet1 = workbook[preset_desc]
    data = sheet1['B31'].value
    driver.find_element_by_xpath(preset_description).clear()
    driver.find_element_by_xpath(preset_description).send_keys(data)
    print("preset description enter done")
    time.sleep(2)

@then("user add channel pair 1 receiver")
def user_add_channel_pair_1_receiver():
    workbook = openpyxl.load_workbook("adder_automation.xlsx")
    sheet1 = workbook['adder']
    data = sheet1['B32'].value
    preset_receiver = Select(driver.find_element_by_xpath(receiver_pair))
    preset_receiver.select_by_visible_text(data)
    print("receiver is added")
    time.sleep(2)


@then("user add channel pair 1 channel")
def user_add_channel_pair_1_channel():
    workbook = openpyxl.load_workbook("adder_automation.xlsx")
    sheet1 = workbook['adder']
    data = sheet1['B33'].value
    preset_channel = Select(driver.find_element_by_xpath(channel_pair))
    preset_channel.select_by_visible_text(data)
    print("channel is added")
    time.sleep(2)


@then("user choose allowed conection mode")
def user_choose_allowed_connection_mode():
    driver.find_element_by_xpath(allowed_connection).click()


@then("user verify the save button")
def user_verify_the_save_button():
    expected = "Save"

    actual = driver.find_element_by_xpath('//*[@id="save_button"]').text

    if expected == actual:
        print("save display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("save is not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False

@then("user save the details")
def user_save_the_details():
    driver.find_element_by_xpath('//*[@id="save_button"]').click()
    print("save the details")
    time.sleep(2)




@given(parsers.parse('user click on "{mode}"'))
def user_click_on_(mode):
    workbook = openpyxl.load_workbook("adder_automation.xlsx")
    sheet1 = workbook[mode]
    data = sheet1['B34'].value
    name = sheet1['B30'].value
    mode1 = "Video-only"
    mode2 = "Shared"
    mode3 = "Exclusive"
    mode4 = "Private"
    # import pdb; pdb.set_trace()
    if(mode1 == data):
        x = 1
        driver.find_element_by_xpath(f"//td[text()='{name}']/..//td[5]/a[{x}]").click()
        print("click on video-only option")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        time.sleep(2)

    elif(mode2 == data):
        x = 2
        driver.find_element_by_xpath(f"//tr[starts-with(@id, 'row_id_')]/td[5]/a[{x}]").click()
        print("click on shared option")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        time.sleep(2)

    elif (mode3 == data):
        x = 3
        driver.find_element_by_xpath(f"//tr[starts-with(@id, 'row_id_')]/td[5]/a[{x}]").click()
        print("click on exclusive option")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        time.sleep(2)

    elif (mode4 == data):
        x = 4
        driver.find_element_by_xpath(f"//tr[starts-with(@id, 'row_id_')]/td[5]/a[{x}]").click()
        print("click on private option")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        time.sleep(2)

    else:
        print("preset channel in not working properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False


@then("user verify receiver heading on top")
def user_verify_reciver_heading_on_top():

    expected = 'RECEIVERS'

    actual = driver.find_element_by_xpath(receiver).text

    if expected == actual:
        print("channel display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("channel not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False


@then("user click on receiver and verify the receiver page is open or not")
def user_click_on_channel_and_verify_the_channel_page_is_open_or_not():
    driver.find_element_by_xpath(receiver).click()
    print("clicked on receiver")
    time.sleep(2)
    try:
        assert "t=rx" in driver.current_url
    finally:
        if (AssertionError):
            allure.attach(driver.get_screenshot_as_png(), name="Invalid Credential",
                          attachment_type=allure.attachment_type.PNG)


@then("user click on connect to a channel")
def user_click_on_connect_to_a_channel():
    driver.find_element_by_xpath("//span[starts-with(@id, 'connect_link_')]").click()
    print("click on channel connected option")
    time.sleep(2)


@given(parsers.parse('user verify the channel is connect "{p_c_name}"'))
def user_verify_the_channel_is_connect_or_not(p_c_name):
    # import pdb;pdb.set_trace()
    workbook = openpyxl.load_workbook("adder_automation.xlsx")
    sheet1 = workbook[p_c_name]
    data = sheet1['B33'].value
    expected = "This receiver is currently connected to " + data

    actual = driver.find_element_by_xpath(preset_channel_connect).text
    real = actual[:-41]
    print(real)
    # import pdb; pdb.set_trace()
    if expected == real:
        print("channel is connected display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("channel is not connected")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False


@then("user verify dashboard heading")
def user_verify_channel_heading():
    expected = 'DASHBOARD'

    actual = driver.find_element_by_xpath(dashboard).text

    if expected == actual:
        print("dashboard display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("dashboard not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False


@then("user click on dashboard")
def user_click_on_dashboard():
    driver.find_element_by_xpath(dashboard).click()
    print("clicked on dashboard")
    time.sleep(3)


@then("user verify active connection heading")
def user_verify_active_connection_heading():

    expected = 'Active Connections'

    actual = driver.find_element_by_xpath(preset_active_connection).text
    # import pdb; pdb.set_trace()
    if expected == actual:
        print("Active Connection tab display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("Active Connections tab not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False

@then("user click on active connection tab")
def user_click_on_active_connection_tab():
    driver.find_element_by_xpath(preset_active_connection).click()
    print("clicked on active connection tab")
    time.sleep(3)



@then("user verify the channel name is same which we created or not in active connection")
def user_verify_the_channel_name_is_same_which_we_created_or_not_in_active_connection(p_c_name):
    workbook = openpyxl.load_workbook("adder_automation.xlsx")
    sheet1 = workbook[p_c_name]
    data = sheet1['B33'].value
    expected = data

    actual = driver.find_element_by_xpath(v_preset_channel_name).text
    if expected == actual:
        print("same channel name is display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("same channel name is not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False

@then("user verify preset name is same which we created or not in active connection")
def user_verify_prset_name_is_same_which_we_created_or_not_in_active_connection(preset_name1):
    workbook = openpyxl.load_workbook("adder_automation.xlsx")
    sheet1 = workbook[preset_name1]
    data = sheet1['B30'].value
    expected = data

    actual = driver.find_element_by_xpath(v_preset_name).text
    if expected == actual:
        print("same preset name is display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("same preset name is not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False


@then("user verify channel is connect with same reciever or not in active connection")
def user_verify_channel_is_connect_with_same_reciever_or_not_in_active_connection(p_c_name):
    driver.find_element_by_xpath(v_preset_reciver_name_active_connection).click()
    # import pdb;pdb.set_trace()
    workbook = openpyxl.load_workbook("adder_automation.xlsx")
    sheet1 = workbook[p_c_name]
    data = sheet1['B33'].value
    expected = "This receiver is currently connected to "+ data

    actual = driver.find_element_by_xpath(v_preset_msg).text
    real = actual[:-41]
    print(real)
    # import pdb; pdb.set_trace()
    if expected == real:
        print("channel is connected with same receiver is display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("channel is not connected with same receiver")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False



@then("user verify connection log heading")
def user_verify_connection_log_heading():

    expected = 'Connection Log'

    actual = driver.find_element_by_xpath(preset_connection_log).text

    if expected == actual:
        print("Connection Log tab display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("Connection Log tab not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False


@then("user click on connection log tab")
def user_click_on_connection_log_tab():
    driver.find_element_by_xpath(preset_connection_log).click()
    print("clicked on Connection Log tab")
    time.sleep(5)



@then("user verify the channel name is same which we created or not in connection log")
def user_verify_the_channel_name_is_same_which_we_created_or_not_in_connection_log(p_c_name):
    workbook = openpyxl.load_workbook("adder_automation.xlsx")
    sheet1 = workbook[p_c_name]
    data = sheet1['B33'].value
    expected = data

    actual = driver.find_element_by_xpath(v_preset_cname_connection_log).text

    if expected == actual:
        print("same channel name is display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("same channel name is not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False


@then("user verify preset name is same which we created or not in connection log")
def user_verify_prset_name_is_same_which_we_created_or_not_in_connection_log(preset_name1):
    workbook = openpyxl.load_workbook("adder_automation.xlsx")
    sheet1 = workbook[preset_name1]
    data = sheet1['B30'].value
    expected = data

    actual = driver.find_element_by_xpath(v_preset_name_connection_log).text
    if expected == actual:
        print("same preset name is display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("same preset name is not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False


@then("user verify channel is connect with same reciever or not in connection log")
def user_verify_channel_is_connect_with_same_reciever_or_not_in_connection_log(p_c_name):
    driver.find_element_by_xpath(v_preset_reciver_name_connection_log).click()
    # import pdb;pdb.set_trace()
    workbook = openpyxl.load_workbook("adder_automation.xlsx")
    sheet1 = workbook[p_c_name]
    data = sheet1['B33'].value
    expected = "This receiver is currently connected to " + data

    actual = driver.find_element_by_xpath(v_preset_msg).text
    real = actual[:-41]
    print(real)
    # import pdb; pdb.set_trace()
    if expected == real:
        print("channel is connected with same receiver is display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("channel is not connected with same receiver")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False


@then("user click on channel disconnect")
def user_click_on_channel_disselect():
    driver.find_element_by_xpath(preset_channel_disconnect).click()
    print("channel sucessfully disselect")
    allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                  attachment_type=allure.attachment_type.PNG)
    time.sleep(5)


@then("user delete the preset")
def user_delete_the_preset():
    workbook = openpyxl.load_workbook("adder_automation.xlsx")
    sheet1 = workbook["adder"]
    data = sheet1['B30'].value
    driver.find_element_by_xpath(f"//td[text()='{data}']/../td[6]/a[3]").click()
    print("click on preset delete option")
    time.sleep(1)


@then("user verify delete option come or not")
def user_verify_delete_option_come_or_not():
    expected = "Delete"

    actual = driver.find_element_by_xpath(preset_final_delete).text

    if expected == actual:
        print("delete option is display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("delete option is not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False

@then("user click on delete option")
def user_click_on_delete_option():
    driver.find_element_by_xpath(preset_final_delete).click()
    print("click on final delete")
    time.sleep(1)
    allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                  attachment_type=allure.attachment_type.PNG)


@then("user click on dashboard")
def user_click_on_dashboard():
    driver.find_element_by_xpath(dashboard).click()
    print("clicked on dashboard")
    allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                  attachment_type=allure.attachment_type.PNG)

