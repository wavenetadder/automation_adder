import openpyxl
from pytest_bdd import scenario, given, when, then, scenarios, parsers
import allure
import time
from pagelocators.adder_common import *
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from tests_1.testcases.WebDeiverFactory import driver

scenarios('../features/tx_reboot.feature')



@then("user verify transmitter heading")
def user_verify_transmitter_heading():
    expected = 'TRANSMITTERS'

    actual = driver.find_element_by_xpath(transmitter).text

    if expected == actual:
        print("transmitter display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                          attachment_type=allure.attachment_type.PNG)

    else:
        print("transmitter not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                          attachment_type=allure.attachment_type.PNG)
        assert False

@then("user click on transmitter and verify the transmitter page is open or not")
def user_click_on_transmitter_and_verify_the_transmitter_page_is_open_or_not():
    driver.find_element_by_xpath(transmitter).click()
    print("clicked on transmitter")
    time.sleep(2)
    try:
        assert "t=tx" in driver.current_url
    finally:
        if (AssertionError):
            allure.attach(driver.get_screenshot_as_png(), name="Invalid Credential",
                            attachment_type=allure.attachment_type.PNG)


@then("user click on the reboot option")
def user_click_on_the_reboot_option():
    workbook = openpyxl.load_workbook("adder_automation.xlsx")
    sheet1 = workbook['adder']
    data = sheet1['B59'].value
    driver.find_element_by_xpath(f"//td[text()='{data}']/../td[11]/span[2]/a[1]/img").click()
    print("click on the reboot options")
    time.sleep(5)

@then("user verify reboot button is appear or not")
def user_verify_reboot_button_is_appear_or_not():

    expected = 'Reboot'
    actual = driver.find_element_by_xpath('//*[@id="reboot_device_confirm_link"]').text
    if expected == actual:
        print("reboot display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                          attachment_type=allure.attachment_type.PNG)

    else:
        print("reboot not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                          attachment_type=allure.attachment_type.PNG)
        assert False


@then("user click on reboot button")
def user_click_on_reboot_button():
    driver.find_element_by_xpath('//*[@id="reboot_device_confirm_link"]/span/img').click()
    print("click on reboot button done")
    time.sleep(70)


@then("user verify dashboard heading")
def user_verify_channel_heading():
    expected = 'DASHBOARD'

    actual = driver.find_element_by_xpath(dashboard).text

    if expected == actual:
        print("dashboard display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("dashboard not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False


@then("user click on dashboard")
def user_click_on_dashboard():
    driver.find_element_by_xpath(dashboard).click()
    print("clicked on dashboard")
    time.sleep(3)

#
# @then("user verify tx reboot done or not")
# def user_verify_tx_reboot_done_or_not():
#
#     expected = "1 transmitter currently rebooting"
#     # expected = "1 device currently being upgraded with new firmware"
#     actual = driver.find_element_by_xpath("//span[@class='message_box mb_yellow error_message']").text
#     # import pdb; pdb.set_trace()
#     if expected == actual:
#         print("tx is rebooting...")
#         element = WebDriverWait(driver, 300).until(
#             EC.invisibility_of_element_located((By.XPATH, "//span[@class='message_box mb_yellow error_message']")))
#         print("tx is reboot")
#         assert True
#         allure.attach(driver.get_screenshot_as_png(), name='screenshot',
#                       attachment_type=allure.attachment_type.PNG)
#
#     else:
#         print("tx is not reboot")
#         allure.attach(driver.get_screenshot_as_png(), name='screenshot',
#                       attachment_type=allure.attachment_type.PNG)
#         assert False
#
# @then(parsers.parse('user verify tx reboot done or not"{tx_reboot}"'))
# def user_verify_upgrade_selected_transmitter_is_shown_or_not(rx_tx_up_down):
#     workbook = openpyxl.load_workbook("adder_automation.xlsx")
#     sheet1 = workbook['adder']
#     data = sheet1['B60'].value
#     expected = data + " device currently being upgraded with new firmware"
#     # expected = "1 device currently being upgraded with new firmware"
#     actual = driver.find_element_by_xpath("//span[@class='message_box mb_yellow error_message']").text
#     # import pdb; pdb.set_trace()
#     if expected == actual:
#         print("firmware is upgrading...")
#         element = WebDriverWait(driver, 300).until(
#             EC.invisibility_of_element_located((By.XPATH, "//span[@class='message_box mb_yellow error_message']")))
#         print("frimware upgrade")
#         assert True
#         allure.attach(driver.get_screenshot_as_png(), name='screenshot',
#                       attachment_type=allure.attachment_type.PNG)
#
#     else:
#         print("frimware not upgrade")
#         allure.attach(driver.get_screenshot_as_png(), name='screenshot',
#                       attachment_type=allure.attachment_type.PNG)
#         assert False