import openpyxl
from pytest_bdd import scenario, given, when, then, scenarios, parsers
import allure
import time
from pagelocators.adder_common import *
from tests_1.testcases.WebDeiverFactory import driver

scenarios('../features/statistics_page.feature')

@then("user verify dashboard heading")
def user_verify_channel_heading():
    expected = 'DASHBOARD'

    actual = driver.find_element_by_xpath(dashboard).text

    if expected == actual:
        print("dashboard display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("dashboard not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False


@then("user click on dashboard")
def user_click_on_dashboard():
    driver.find_element_by_xpath(dashboard).click()
    print("clicked on dashboard")
    time.sleep(2)

@then("user verify statistics option")
def user_verify_setting_option():

    expected = 'STATISTICS'

    actual = driver.find_element_by_xpath(statistics).text

    if expected == actual:
        print("statistics display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("statistics is not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False

@then("user click on statistics option and verify statistics page is open or not")
def user_click_on_setting_option_and_verify_setting_page_is_open_or_not():
    driver.find_element_by_xpath(statistics).click()
    print("clicked on statistics page")
    time.sleep(2)
    try:
        assert "statistics" in driver.current_url
    finally:
        if (AssertionError):
            allure.attach(driver.get_screenshot_as_png(), name="Invalid Credential",
                          attachment_type=allure.attachment_type.PNG)

@then('user click on enable all devices')
def user_click_on_enable_all_devices():
    driver.find_element_by_xpath("//a[text()='Enable All']").click()
    print("clicked on enable all devices ")


@then(parsers.parse('user verify the tx name on statistics page "{s_tx}"'))
def user_verify_the_rx_name_on_statistics_page(s_tx):
    workbook = openpyxl.load_workbook("adder_automation.xlsx")
    sheet1 = workbook[s_tx]
    data = sheet1['B28'].value
    tx_name = data

    actual = driver.find_element_by_xpath(f"//a[text()=' {data} ']").text
    # import pdb; pdb.set_trace()
    if tx_name == actual:
        print("tx display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("tx is not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False

@then("user click on tx option and verify tx statistics page is open or not")
def user_click_on_rx_option_and_verify_rx_statistics_page_is_open_or_not(s_tx):
    workbook = openpyxl.load_workbook("adder_automation.xlsx")
    sheet1 = workbook[s_tx]
    data = sheet1['B28'].value
    driver.find_element_by_xpath(f" //a[text()=' {data} ']").click()
    print("clicked on tx option")
    time.sleep(2)
    try:
        assert "page=stats.html" in driver.current_url
        time.sleep(5)
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        driver.back()
        time.sleep(2)
    finally:
        if (AssertionError):
            allure.attach(driver.get_screenshot_as_png(), name="Invalid Credential",
                          attachment_type=allure.attachment_type.PNG)



@then(parsers.parse('user verify the rx name on statistics page "{s_rx}"'))
def user_verify_the_rx_name_on_statistics_page(s_rx):
    workbook = openpyxl.load_workbook("adder_automation.xlsx")
    sheet1 = workbook[s_rx]
    data = sheet1['B29'].value
    rx_name = data

    actual = driver.find_element_by_xpath(f"//a[text()=' {data} ']").text
    # import pdb; pdb.set_trace()
    if rx_name == actual:
        print("rx display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("rx is not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False

@then("user click on rx option and verify rx statistics page is open or not")
def user_click_on_rx_option_and_verify_rx_statistics_page_is_open_or_not(s_rx):
    workbook = openpyxl.load_workbook("adder_automation.xlsx")
    sheet1 = workbook[s_rx]
    data = sheet1['B29'].value
    driver.find_element_by_xpath(f"//a[text()=' {data} ']").click()
    print("clicked on rx option")
    time.sleep(2)
    try:
        assert "page=stats.html" in driver.current_url
        time.sleep(5)
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        driver.back()
    finally:
        if (AssertionError):
            allure.attach(driver.get_screenshot_as_png(), name="Invalid Credential",
                          attachment_type=allure.attachment_type.PNG)


 