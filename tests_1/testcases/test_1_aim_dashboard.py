from pytest_bdd import given, then, scenarios, parsers
import allure
import time
from pagelocators.adder_common import *
from pagelocators.testcase1 import *
from tests_1.testcases.WebDeiverFactory import driver
import openpyxl

scenarios('../features/aim_dashboard.feature')

@given("user is in aim login page or not")
def user_is_in_aim_login_page_or_not():
    # driver.implicitly_wait(30)
    expected_login_screen = 'AdderLink Infinity Management Suite'
    actual_login_screen = driver.find_element_by_xpath(login_screen).text

    if expected_login_screen == actual_login_screen:
        print("user is in aim login page")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("user is not is aim login page")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False


@given(parsers.parse('user enter the username "{user_id}"'))
def user_should_enter_username(user_id):
    workbook = openpyxl.load_workbook("adder_automation.xlsx")
    sheet1 = workbook[user_id]
    data = sheet1['B5'].value
    email = driver.find_element_by_xpath(username).clear()
    email = driver.find_element_by_xpath(username).send_keys(data)

@given(parsers.parse('user enter the password "{user_password}"'))
def user_should_enter_username(user_password):
    workbook = openpyxl.load_workbook("adder_automation.xlsx")
    sheet1 = workbook[user_password]
    data = sheet1['B6'].value
    password = driver.find_element_by_xpath(Upassword).clear()
    password = driver.find_element_by_xpath(Upassword).send_keys(data)

@then("user click on login button")
def user_click_on_login_button():
    driver.find_element_by_xpath(login_button).click()
    time.sleep(2)
    # assert "admin" in driver.current_url

@then("user verify the login done or not")
def user_verify_the_login_done_or_not():
    expected = 'Logout'

    actual = driver.find_element_by_xpath(login_verify).text

    if expected == actual:

        print("user is login sucessfully")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("user is not login sucessfully")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False

@then("user verify transmitter heading")
def user_verify_transmitter_heading():

    expected = 'TRANSMITTERS'

    actual = driver.find_element_by_xpath(transmitter).text

    if expected == actual:
        print("transmitter display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("transmitter not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False

@then("user click on transmitter and verify the transmitter page is open or not")
def user_click_on_transmitter_and_verify_the_transmitter_page_is_open_or_not():
    driver.find_element_by_xpath(transmitter).click()
    print("clicked on transmitter")
    time.sleep(2)
    try:
        assert "t=tx" in driver.current_url
    finally:
        if (AssertionError):
            allure.attach(driver.get_screenshot_as_png(), name="Invalid Credential",
                          attachment_type=allure.attachment_type.PNG)


@then("user check the transmitter state")
def user_check_the_transmitter_state():
    click = driver.find_element_by_xpath(trans_state).click()
    actual_path = driver.find_element_by_xpath(trans_state).get_attribute("src")
    path = actual_path[46:]
    print(path)
    online_mode = "tick.png"
    offline_mode = "cross.png"
    if path == online_mode:
        print("transmitter display online")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
    elif path == offline_mode:
        print("transmitter display ofline")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
    else:
        print("There is something wrong transmitter not display online neither offline")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False


@then("user verify receiver heading")
def user_verify_reciver_heading():

    expected = 'RECEIVERS'

    actual = driver.find_element_by_xpath(receiver).text

    if expected == actual:
        print("channel display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("channel not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False

@then("user click on receiver and verify the receiver page is open or not")
def user_click_on_receiver_and_verify_the_receiver_page_is_open_or_not():
    driver.find_element_by_xpath(receiver).click()
    print("clicked on receiver")
    time.sleep(2)
    try:
        assert "t=rx" in driver.current_url
    finally:
        if (AssertionError):
            allure.attach(driver.get_screenshot_as_png(), name="Invalid Credential",
                          attachment_type=allure.attachment_type.PNG)

@then("user verify receiver state")
def user_check_the_receiver_state():
    click = driver.find_element_by_xpath(receiver_state).click()
    actual_path = driver.find_element_by_xpath(receiver_state).get_attribute("src")
    path = actual_path[46:]
    # print(path)
    online_mode = "tick.png"
    offline_mode = "cross.png"
    if path == online_mode:
        print("receiver display online")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
    elif path == offline_mode:
        print("receiver display ofline")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
    else:
        print("There is something wrong receiver not display online neither offline")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False


@then("user verify dashboard heading")
def user_verify_channel_heading():
    expected = 'DASHBOARD'

    actual = driver.find_element_by_xpath(dashboard).text

    if expected == actual:
        print("dashboard display properly")
        assert True
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

    else:
        print("dashboard not display properly")
        allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)
        assert False


@then("user click on dashboard verify the receiver page is open or not")
def user_click_on_dashboard_verify_the_receiver_page_is_open_or_not():
    driver.find_element_by_xpath(dashboard).click()
    print("clicked on dashboard")
    time.sleep(2)
    try:
        assert "index.php" in driver.current_url
    finally:
        if (AssertionError):
            allure.attach(driver.get_screenshot_as_png(), name="Invalid Credential",
                          attachment_type=allure.attachment_type.PNG)



















